Nous voudrions rendre hommage à l'un de nos reporters, introuvable actuellement, qui est allé au péril de sa vie, enquêter sur des faits étranges.
Tout a commencé avec l'arrivée de Taurus sur nos terres. Cette créature, échappée du portail sur le monde sombre, a été brièvement rencontrée par nombre de maitres Dinoz, et grâce à l'aide de Monsieur Bao Bob, cette menace fut repoussée assez rapidement.

Néanmoins, l'agitation du monde sombre faisait déjà peur. Les barrières royales nécessitent une quantité phénoménale de mana (produit lors des dernières GDM) mais ne suffisent apparemment plus, comme en témoignent les chasses aux monstres organisées de plus en plus fréquemment pour pallier aux invasions récurrentes de pirhan-oz et autre garousaures.

Notre reporter voulait dénoncer cette méthode "couvercle" mise en place dans nos royaumes pour protéger ses habitants. Mais depuis son départ, il n'a plus donné signe de vie...

Jusqu'à ce fameux cliché reçu mystérieusement par mail ce matin, ainsi que la message audio suivant :

" Mercredi 21 mars, 15 heures.

Tout ce que nous croyions connaître sera bouleversé par ma découverte.

Il y a plusieurs années, nous ne connaissions pas l'existence des Kabukis, et nous avions oublié les évènements de Jazz.

La découverte de Nimbao est encore plus récente. Et heureusement que nous avions pu compter sur des alliés de poids tels que Mandragore. Et encore ! Les dorogons ne nous ont pas attaqués. Ce fut de la chance, rien de plus...

Si ce que je pense est vrai, même lui ne suffirait pas à nous aider. Nous ne sommes pas prêts.
Le monde sombre fait peur, Cauchemesh encore plus.
Les Gardiens ne pourront pas museler éternellement cette menace, et la seule chose connue de Cauchemesh jusqu'à présent, c'est le santaz. Or, tout Dinoland tremble en entendant ce nom. Pourquoi ?

Pourquoi imposer des barrières à ces contrées si ce n'est pour nous protéger ? Mais de quoi ? D'une véritable menace, ou de notre ignorance ?

"Nous ne sommes pas prêts". Tels furent les seuls mots de Bao Bob, du Maitre élémentaire, de Mandragore. Ils sont unanimes.

Connaître la vérité, c'est mon métier. Derrière cette barrière, il me sera impossible de communiquer. Mais croyez-moi, je découvrirai ce qui se passe dans ces contrées lointaines, même si je dois briser cette barrière."

Depuis, c'est le silence radio le plus total.

Notre équipe analyse actuellement la photographie pour connaître le lieu exact où elle a été prise. Un canular semble à exclure.

Nous voulons que les autorités nous rassurent sur la solidité et l'utilité des barrières magiques nous protégeant de ces contrées et nous espérons qu'elles nous donneront enfin les moyens de nous y préparer au mieux.

Et le plus tôt sera le mieux : la barrière faiblit dangereusement.