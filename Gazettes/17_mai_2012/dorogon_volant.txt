Mesdames et Messieurs, petits et grands, j'ai l'immense honneur de vous convier à l'ouverture du tout nouvel Hôtel-Restaurant « Le Dorogon Volant » à Dinoplaza, lieu en essor permanent.
Venez y découvrir par centaines ce magnifique édifice approuvé par Leguide Michel et certifié par les « Gîtes de Dinoland » comme étant l'un des meilleurs de sa catégorie.

En effet c’est par des prix accessibles pour toutes et pour tous et par une qualité irréprochable que « Le Dorogon Volant » comblera chacun d’entre vous.

Le Hall d’entrée est magnifique, on distingue des trophées de chasse un peu partout, c’est décidément un bestiaire à lui tout seul. De hautes et majestueuses sculptures dans du basalte à l’effigie de stars dinoviliennes sont présentes. On remarque facilement celle de la chanteuse Riz Nana ou bien du rappeur Emile Nem.

Le sol est entièrement recouvert d’une épaisse touffe de fourrure de Castivore et de Santaz mais l’on nous rassure rapidement qu’aucun mal n’a été fait à ces adorables dinoz, c’est uniquement la tonte de plusieurs d’entre eux qui a permis ce remarquable et soyeux exploit.

Un room service est également présent composé uniquement de Dinoz entraînés par les meilleurs éleveurs de la région, à savoir particulièrement les Moueffes pour le port des Bagages, mais aussi en tant que réceptionnistes, cuisiniers, serveurs mais aussi au nettoyage des chambres et des nombreuses suites car quoi qu’on en dise leur puissante musculature accomplit bien des miracles !

Pour ne pas faire de jaloux « Le Dorogon Volant » à pensé à tout en mettant aussi en avant bien d’autres dinoz dont je vais vous faire l’éloge par la suite chers lecteurs !

Par ailleurs, ce sont des suites d’une grande qualité qui vous donneront la sensation d’être à Nimbao et d’avoir l’impression d’être transporter comme sur un Nuagoz. En effet, des lits spacieux de matières différentes là encore selon vos goûts, allant des lits de Basaltes et/ou de pierres des Steppes pour les plus courageux aux lits créés, modelés et façonnés par l’entreprise « Korgon & Scie » dont les nombreux cadres et montures proviennent de la luxuriante et touristique Forêt de Grumhel.

Quand aux plus « Wanwan » d’entre nous, ce sont des lits doux et soyeux qui attendront d’être lover.

C’est donc par une grande diversité que « Le Dorogon Volant » met la barre haut et ainsi touche un public large.

(A noter que pour les plus petits, des lits à l’effigie de leurs dinoz préférés sont d’ores et déjà disponibles (on ne compte malheureusement pour l’instant que deux races disponibles, dont Le lit « Casti-confort » un poil irritant et le lit suspendu « Nuagoz » qui pourrait donner le vertige à plus d’uns.))

Un Hotêl régulé par la technologie Caushemesh mais nous n’en savons pas plus pour le moment, si ce n’est que l’air condensé viendrait tout droit de cette région et que la chaleur quant à elle serait plutôt exportée en provenance du Mont Tout Chaud. Gare aux rhumes !

Venons-en au plus important, je veux bien entendu parler du restaurant ! Pour tout vous dire j’avais de l’appréhension au début mais finalement je me suis régalé. Pour une grande première, nous n’avons eu malheureusement droit qu’à un petit hors-d’œuvre si j’ose dire.
Car étant récemment ouvert, le restaurant n’a pas pu encore être totalement approvisionné.

(Approvisionné au passage par des produits frais du Port de Prêche, et un service de livraison inégalé de chez Broc, l’inventeur de génie.)

Sans plus tarder, voilà quelques plats dont nous avons pu avoir l’immense joie de goûter :

- Mousse de Mérou Lujidané sur son lit de Feuilles de Pelinae quatre saisons.

- Œufs de Ptéroz pochés avec mouillettes de Phalisk blanc.

- Steaks de Korgons cuit au feu de bois de la Forêt de Gruhmel parsemés de copeaux de corne en chocolat.

- Tarte aux Goupignons avec son soupçon de crème de Jouvence.

Quand aux rafraichissements nous avons eu droit à un excellent Jus de Pampleboum et pour les amateurs de grand cru, un Vin Mouktiz de 1978 vieilli en fût de dévoreuse (personnellement je n’y ai pas touché).

Le reste de l'équipe n'a pourtant pas lésiné sur la liqueur de Bambooz... bref passons !

Après ce détour culinaire, il est temps pour moi de vous parler des loisirs et divertissements que vous propose « Le Dorogon Volant ».
En effet, l’Hôtel-Restaurant est doté d’une piscine couverte avec une eau thermale encore en provenance du Mont Tout Chaud tempérament chauffée, avec possibilité de nager en harmonie avec Sirain et Hippoclamp prévus à cet effet. our les plus petits, n’oubliez pas la bouée.
Pour entretenir la forme, une salle de sport est aussi au rendez vous, des appareils High-tech là encore signés Caushemesh.

A noter que tout ceci sera possible si vous possédez une carte de Membre.