Le tonnerre gronde, le sol tremble. Très chers lecteurs, aujourd’hui la gazette prend des risques… Préparez-vous à plonger au cœur de la révolution !

Vous avez tous entendu dernièrement l’annonce du roi : alors que les clans attendaient fébrilement un championnat c’est finalement une nouvelle guerre du mana que le roi a décidé d’organiser.

Les premières stupeurs passées, une vague d’indignation s’est élevée et de nombreux maitres dinoz ont tenté de se faire entendre. Beaucoup d’interrogations se sont succédées : pourquoi deux guerres à la suite, pourquoi rompre l’éternel cycle guerre-championnat ? Questions pour le moment sans réponse.

Inquiétudes d’autant plus légitimes qu’il y a peu de temps, le roi en personne était venu demander l’aide des maitres dinoz pour contenir une « invasion » de lichen causé par le trop grand nombre de mana dans les cuves du château.

Nous avons donc enquêté sur ce phénomène qui semble prendre de plus en plus d’ampleur. Pour cela, nous vous proposons de suivre notre enquête à travers le journal de notre toute jeune reporter, Lucy Scale.

(Dans un souci de confidentialité, nous avons modifié le nom des personnes cités.)

« 8 juillet-16h35

L’enquête commence, pour une première je tiens un gros morceau ! Bon impossible d’obtenir quoi que ce soit venant de la cour, silence total, ça cache quelque chose. Même chose du côté des habitants de la place de la fontaine, personne ne semble rien savoir, à croire que cette histoire de révolution n’est qu’invention… Non je dois continuer à chercher, il en va de mon honneur de journaliste ! »

« 9 juillet- 10h46

Extra, enfin une piste ! Je savais bien que je trouverai quelque chose en allant dans les bas-fonds de Dinoplaza. Quelle chance j’ai eu de croiser cet homme ! Par contre, il a l’air bizarre, je n’ai même pas pu voir son visage, il m’a juste donné cette carte et a disparu dans la foule avant que j’ai n’ai eu le temps de dire quoi que ce soit. Bon il faut que je me rende à l’auberge indiquée, je commence à avoir un peu peur. »

« 9 juillet- 19h37

J’ai encore du mal à y croire, la révolution existe bien ! Enfin pour le moment ce n’est qu’un petit groupe mais j’ai pu reconnaitre certaines personnes, c’est incroyable ! Par contre, je n’ai pas pu découvrir ou les réunions avaient lieu. J’étais à peine arrivée à l’auberge que deux hommes aussi baraqués que des gorilloz m’ont sautés dessus pour me bander les yeux et mes pauvres dinoz n’ont rien pu faire ! Je vais devoir faire sans…
Celui qui s’est présenté comme chef du mouvement se fait appeler Jack, je crois que c’est l’homme que j’ai rencontré ce matin. Il m’a proposé de rejoindre le mouvement, j’ai accepté sans réfléchir, il dégage une aura très spéciale, j’espère ne pas avoir fait une erreur. Il doit me recontacter pour « me mettre à l’épreuve » m’a-t-il dit. J’ai peur mais je suis tellement excitée ! »


Continuez à nous suivre, nous reviendrons bientôt avec la suite de cette chronique, et croyez-moi, vous n’êtes pas au bout de vos surprises !